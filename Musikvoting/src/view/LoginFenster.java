package view;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import database.DatabaseConnection;
import musikvoting.LoginSystem;

public class LoginFenster extends JFrame {

	VotingFenster vf = new VotingFenster();
	private JPanel contentPane;
	private JTextField txtfName;
	private JTextField txtfPasswort;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		DatabaseConnection.ladeAccounts();
		DatabaseConnection.ladeSongs();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFenster frame = new LoginFenster();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginFenster() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 157);
		contentPane = new JPanel() {
			public void paintComponent(Graphics g) {
			Image img = Toolkit.getDefaultToolkit().getImage(  
			LoginFenster.class.getResource("/Bilder/bg-Login.jpg"));  
			g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
			}
		};
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{212, 212, 0};
		gbl_contentPane.rowHeights = new int[]{25, 25, 25, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
	/*	Choice choice = new Choice();
		GridBagConstraints gbc_choice = new GridBagConstraints();
		choice.add("Gast");
		choice.add("DJ");
		gbc_choice.fill = GridBagConstraints.BOTH;
		gbc_choice.insets = new Insets(0, 0, 5, 0);
		gbc_choice.gridx = 1;
		gbc_choice.gridy = 0;
		contentPane.add(choice, gbc_choice);*/
		
		JLabel lblName = new JLabel("Username");
		lblName.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.fill = GridBagConstraints.BOTH;
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 1;
		contentPane.add(lblName, gbc_lblName);
		
		txtfName = new JTextField();
		GridBagConstraints gbc_txtfName = new GridBagConstraints();
		gbc_txtfName.fill = GridBagConstraints.BOTH;
		gbc_txtfName.insets = new Insets(0, 0, 5, 0);
		gbc_txtfName.gridx = 1;
		gbc_txtfName.gridy = 1;
		contentPane.add(txtfName, gbc_txtfName);
		txtfName.setColumns(10);
		
		JLabel lblPasswort = new JLabel("Passwort");
		lblPasswort.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblPasswort = new GridBagConstraints();
		gbc_lblPasswort.fill = GridBagConstraints.BOTH;
		gbc_lblPasswort.insets = new Insets(0, 0, 5, 5);
		gbc_lblPasswort.gridx = 0;
		gbc_lblPasswort.gridy = 2;
		contentPane.add(lblPasswort, gbc_lblPasswort);
		
		txtfPasswort = new JTextField();
		GridBagConstraints gbc_txtfPasswort = new GridBagConstraints();
		gbc_txtfPasswort.insets = new Insets(0, 0, 5, 0);
		gbc_txtfPasswort.fill = GridBagConstraints.BOTH;
		gbc_txtfPasswort.gridx = 1;
		gbc_txtfPasswort.gridy = 2;
		contentPane.add(txtfPasswort, gbc_txtfPasswort);
		txtfPasswort.setColumns(10);
		
		JButton btnLogin = new JButton("login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(LoginSystem.einloggen(txtfName.getText(), txtfPasswort.getText())) {
				//Verschwinden des Fensters
				vf.pack();
				vf.setVisible(true);
				dispose();
			} else {
				JOptionPane.showMessageDialog(null, "Die Anmeldedaten stimmen nicht �ber ein!", "Fehler", JOptionPane.OK_OPTION);
				}
			}
		});
		btnLogin.setHorizontalAlignment(SwingConstants.LEADING);
		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.fill = GridBagConstraints.VERTICAL;
		gbc_btnLogin.insets = new Insets(0, 0, 0, 5);
		gbc_btnLogin.gridx = 0;
		gbc_btnLogin.gridy = 3;
		contentPane.add(btnLogin, gbc_btnLogin);
		
		JButton btnRegristrieren = new JButton("Registrieren");
		btnRegristrieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtfName.getText().equals("") || txtfPasswort.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Bitte geben Sie sowohl Kontoname als auch ein Passwort ein!", "Fehler", JOptionPane.OK_OPTION);
				} else { 
				if(LoginSystem.hinzufuegen(txtfName.getText(), txtfPasswort.getText(), "Gast")) {
				//Verschwinden des Fensters
				vf.pack();
				vf.setVisible(true);
				dispose();
				} else {
					JOptionPane.showMessageDialog(null, "Account bereits vorhanden!", "Fehler", JOptionPane.OK_OPTION);
				}
				}
			}
		});
		GridBagConstraints gbc_btnRegristrieren = new GridBagConstraints();
		gbc_btnRegristrieren.gridx = 1;
		gbc_btnRegristrieren.gridy = 3;
		contentPane.add(btnRegristrieren, gbc_btnRegristrieren);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
		
		JFrame container = new JFrame("Space Invaders");
		JLabel label = new JLabel(new ImageIcon("/Bilder/GUI-Login.jpg"));
		label.setLayout(new BorderLayout());
		container.setContentPane(label);
	}
}
