package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import musikvoting.Playlist;

public class AddFenster extends JFrame {

	static JFrame vf = new JFrame();
	private JPanel contentPane;
	private JTextField txtfTitel;
	private JTextField txtfInterpret;
	private JTextField txtfGenre;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddFenster frame = new AddFenster(vf);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddFenster(JFrame j) {
		vf = j;
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 575, 140);
		contentPane = new JPanel() {
			public void paintComponent(Graphics g) {
			Image img = Toolkit.getDefaultToolkit().getImage(LoginFenster.class.getResource("/Bilder/bg-add.jpg"));  
			g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
			}
		};
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel inputPane = new JPanel();
		inputPane.setBackground(new Color(204, 0, 204));
		inputPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.add(inputPane);
		inputPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblTitel = new JLabel("Titel:");
		inputPane.add(lblTitel);
		
		txtfTitel = new JTextField();
		inputPane.add(txtfTitel);
		txtfTitel.setColumns(10);
		
		JLabel lblInterpret = new JLabel("Interpret:");
		inputPane.add(lblInterpret);
		
		txtfInterpret = new JTextField();
		inputPane.add(txtfInterpret);
		txtfInterpret.setColumns(10);
		
		JLabel lblGenre = new JLabel("Genre:");
		inputPane.add(lblGenre);
		
		txtfGenre = new JTextField();
		inputPane.add(txtfGenre);
		txtfGenre.setColumns(10);
		
		JPanel buttonPane = new JPanel();
		buttonPane.setBackground(new Color(0, 0, 0));
		buttonPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.add(buttonPane);
		buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnAbbrechen_1 = new JButton("Abbrechen");
		btnAbbrechen_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vf.pack();
				vf.setVisible(true);
				dispose();
			}
		});
		buttonPane.add(btnAbbrechen_1);
		
		JButton btnHinzufuegen_1 = new JButton("Hinzuf\u00FCgen");
		btnHinzufuegen_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(Playlist.hinzufuegen(txtfTitel.getText(), txtfInterpret.getText(), txtfGenre.getText())) {
				txtfTitel.setText("");
				txtfInterpret.setText("");
				txtfGenre.setText("");
				dispose();
				vf.pack();
				vf.setVisible(true);
				}else {
					JOptionPane.showMessageDialog(null, "Song bereits vorhanden!", "Fehler", JOptionPane.OK_OPTION);
				}
			}
		});
		buttonPane.add(btnHinzufuegen_1);
		
	}

}
