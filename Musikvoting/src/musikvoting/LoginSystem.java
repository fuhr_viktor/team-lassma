package musikvoting;

import java.util.LinkedList;


import database.DatabaseConnection;

public class LoginSystem {
	static LinkedList<Account> accountListe = new LinkedList<Account>();
	static Account eingeloggt;

	public static void hinzufuegenDB(String name, String passwort, String typ){
		accountListe.add(new Account(name, passwort, typ));
	}

	public static boolean hinzufuegen(String name, String passwort, String typ){
		if(DatabaseConnection.befehl("INSERT INTO T_Account VALUES('"+ name + "','"+ passwort +"','" + typ + "');")) {
		accountListe.add(new Account(name, passwort, typ));
		return true;
		} else {
			return false;
		}
	}
	
	public static boolean entfernen(int index) {
		accountListe.remove(index);
		return true;
	}
	
	public static boolean einloggen(String name, String passwort) {
		for(int i = 0;i < accountListe.size(); i++) {
			if(name.equals(accountListe.get(i).getName())) {
				//System.out.println("gefunden");
				if(passwort.equals(accountListe.get(i).getPasswort())) {
					eingeloggt = accountListe.get(i);
					return true;
				} else {
					//System.out.println("falsches Passwort");
					return false;
				}
			}
			
		} 
		//System.out.println("nicht gefunden"); 
		return false;
	}

	public static LinkedList<Account> getAccountListe() {
		return accountListe;
	}

	public static Account getEingeloggt() {
		return eingeloggt;
	}
	
	
}
