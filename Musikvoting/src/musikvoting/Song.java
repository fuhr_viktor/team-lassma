package musikvoting;

public class Song {
	String titel;
	String interpret;
	String genre;
	int voteAnz;
	
	public Song(String titel, String interpret, String genre) {
		this.titel = titel;
		this.interpret = interpret;
		this.genre = genre;
		this.voteAnz = 0;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getInterpret() {
		return interpret;
	}

	public void setInterpret(String interpret) {
		this.interpret = interpret;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getVoteAnz() {
		return voteAnz;
	}

	public void setVoteAnz(int voteAnz) {
		this.voteAnz = voteAnz;
	}
	
	public void voten() {
		setVoteAnz(this.getVoteAnz() + 1);
	}

	public void votesReset() {
		setVoteAnz(0);
	}
	
	@Override
	public String toString() {
		return "Song: " + titel + " von " + interpret + ", Genre:" + genre + " Votes:" + voteAnz + "\n";
	}
}
