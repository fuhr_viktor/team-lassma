package musikvoting;

import java.util.LinkedList;

import javax.swing.JOptionPane;

import database.DatabaseConnection;

public class Playlist {
	static LinkedList<Song> songListe = new LinkedList<Song>();

	public Playlist() {
		songListe.clear();
	}

	public static void sortieren() {
		int letzteZahl = songListe.size();
		Song temp;
		for (int j = 0; j < songListe.size(); j++) {
			for (int i = 0; i < letzteZahl - 1; i++) {
				if (songListe.get(i).getVoteAnz() < songListe.get(i + 1).getVoteAnz()) {
					temp = songListe.get(i);
					songListe.set(i, songListe.get(i + 1));
					songListe.set(i + 1, temp);
				}

			}
			letzteZahl--;
		}
	}

	public static void hinzufuegen(Song song) {
		songListe.add(song);
		sortieren();
	}

	public static boolean hinzufuegenDB(String name, String interpret, String genre) {
		Song song = new Song(name, interpret, genre);
		songListe.add(song);
		return true;
	}

	public static boolean hinzufuegen(String name, String interpret, String genre) {
		if (DatabaseConnection
				.befehl("INSERT INTO T_Songs VALUES('" + name + "','" + interpret + "','" + genre + "');")) {
			Song song = new Song(name, interpret, genre);
			songListe.add(song);
			return true;
		} else {
			return false;
		}
	}

	public static void entfernen(int position) {
		songListe.remove(position);
	}

	public static void entfernen(Song song) {
		songListe.remove(song);
	}

	public static Song nextSong() {
		sortieren();
		return songListe.getFirst();
	}

	public static int getLaenge() {
		return songListe.size();
	}

	public static Song getSong(int i) {
		return songListe.get(i);
	}

	public static Song getSong(String titel) {
		for (int i = 0; i < songListe.size(); i++) {
			if (songListe.get(i).getTitel().equals(titel))
				return songListe.get(i);
		}
		return null;
	}

	public static String string() {
		try {
			String rtn = "" + songListe.get(0);
			for (int i = 1; i < songListe.size(); i++) {
				rtn += songListe.get(i);
			}
			return rtn;
		} catch (Exception e) {
			return "Playlist ist leer";
		}

	}

	public static String getTop5() {
		try {
			String rtn = "";
		if(songListe.get(0).getVoteAnz()>0) rtn = ""+ songListe.get(0);
		int laenge;
	
		if (songListe.size() < 5) laenge = songListe.size();
		else laenge = 5;
		
		for(int i = 1; i < laenge; i++) {
			if(songListe.get(i).getVoteAnz() > 0)
			rtn += songListe.get(i);
		}
		return rtn;
		}catch (Exception e) {
			return "Playlist ist leer";
		}
			
	}
}
