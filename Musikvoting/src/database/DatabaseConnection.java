package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import musikvoting.LoginSystem;
import musikvoting.Playlist;

public class DatabaseConnection {
	private static String driver = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://localhost/musikvoting";
	private static String user = "root";
	private static String password = "";
	
	public static boolean befehl(String befehl){
		try {
			//Jdbc-Treiber laden 
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			stmt.executeUpdate(befehl);
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}	
	
	public static void ladeAccounts() {
		Connection con;
		try {
			con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement(); 		
			ResultSet rs = stmt.executeQuery("SELECT * FROM t_account"); 
			while(rs.next()) {  						   
					LoginSystem.hinzufuegenDB(rs.getString("p_name"), rs.getString("Kennwort"), rs.getString("typ"));
				}
			} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void ladeSongs() {
		Connection con;
		try {
			con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement(); 		
			ResultSet rs = stmt.executeQuery("SELECT * FROM t_songs"); 
			while(rs.next()) {  						   
					Playlist.hinzufuegenDB(rs.getString("p_name"), rs.getString("p_interpret"), rs.getString("genre"));
				}
			} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*public static void main(String[] args) {
		DatabaseConnection.befehl("insert into T_Songs values('ja2','nein2','jain2')");
		DatabaseConnection.ladeAccounts();
		DatabaseConnection.ladeSongs();
		System.out.println(LoginSystem.getAccountListe());
		System.out.println(Playlist.string());
		
	}*/
	
}
